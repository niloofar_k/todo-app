import React from 'react';
import {KeyboardAvoidingView, Switch} from 'react-native';
import {
    Divider,
    View,
    Row,
    Button,
    CheckBox,
    ScrollView,
    TextInput,
} from 'avijeh-ui-kit';
import Text from '../../../Text';
import {styles} from '../Authentication.Style';
import DescriptionContainer from './SubComponents/DescriptionContainer';
import FooterContainer from './SubComponents/FooterContainer';
import Translate from '../Lang';

const Register = (props) => {
    // todo: line 27 checkbox checked background
    // todo: Switch button style
    return (
        <KeyboardAvoidingView style={styles.keyboardAvoid}>
            <ScrollView
                contentContainerStyle={styles.screenContainer}
                keyboardShouldPersistTaps={'always'}>
                <DescriptionContainer imgFolder={'RegisterImage'}/>
                <View style={styles.inputsContainer}>
                    <TextInput
                        placeholder={Translate('enterMobileNumber')}
                        keyboardType={'phone-pad'}
                        style={styles.mobileNumberText}
                        containerStyle={styles.mobileNumber}
                        placeholderTextColor={'#6d7278'}
                    />
                    <Row transparent style={styles.registerContainer}>
                        <CheckBox
                            CheckboxStyle={styles.introduceCheckBox}
                            type={'warning'}
                            color={'#fff'}>
                            <Text
                                style={styles.introducer}>{props.introducer}</Text>
                        </CheckBox>
                    </Row>
                </View>

                <Divider/>

                <View style={styles.inputsContainer}>
                    <Row transparent style={styles.agreeContainer}>
                        <Text category={'p1'} style={styles.agreeText}>
                            {props.agree}
                            <Text
                                style={styles.readableRules}
                                onPress={() => props.navigation.navigate(
                                    'Rules')}>
                                {props.rules}
                            </Text>
                            {props.service}
                        </Text>
                        <Switch
                            onTintColor={'#f18d22'}
                            thumbTintColor={'#f18d22'}
                            tintColor={'#bbb'}
                        />
                    </Row>
                    <Button
                        style={styles.loginButton}
                        TextStyle={styles.buttonText}
                        title={Translate('createAccount')}
                        onPress={() => props.navigation.navigate(
                            'CreatePassword')}
                    />
                </View>
                <FooterContainer
                    title={Translate('enterAccount')}
                    footerButtonAction={() => props.navigation.navigate(
                        'Login')}
                />
            </ScrollView>
        </KeyboardAvoidingView>
    );
};

export default Register;

Register.defaultProps = {
    introducer: 'کد معرف دارید‌ ؟',
    service: ' این سرویس موافقم',
    agree: 'من با تمام',
    rules: ' شرایط و قوانین',
};
