import React from 'react';
import {View, Button, Row} from 'avijeh-ui-kit';
import Text from '../../../../Text';
import {styles} from '../../Authentication.Style';

const FooterContainer = ({
    title,
    buttonTextStyle,
    buttonStyle,
    footNote,
    footNoteStyle,
    footerButtonAction,
    touchableText,
    ...props
}) => {
    //todo: shadow style in android does not work

    return (
        <View style={styles.footerContainer}>
            <Button
                style={[styles.footerButton, buttonStyle]}
                TextStyle={[styles.buttonText, buttonTextStyle]}
                title={title}
                onPress={footerButtonAction}
            />
            {footNote ? (
                <Text
                    style={[styles.footerText, footNoteStyle]}
                    onPress={touchableText ? footerButtonAction : () => null}>
                    {footNote}
                </Text>
            ) : (
                <Row transparent>
                    <Text style={styles.avijeh}>Avijeh</Text>
                    <Text style={styles.footerText}>{props.powered}</Text>
                </Row>
            )}
        </View>
    );
};

export default FooterContainer;

FooterContainer.defaultProps = {
    powered: `Powered By `,
};
