import React from 'react';
import {Image} from 'react-native';
import {Divider, View} from 'avijeh-ui-kit';
import Text from '../../../../Text';
import {styles} from '../../Authentication.Style';
import {imgAddress} from'../assets';
const DescriptionContainer = ({
    descriptionText,
    subDescriptionText,
    imgFolder,
}) => {
    return (
        <View style={styles.descriptionContainer}>
            <Image style={styles.imageSize} source={imgAddress[imgFolder]}/>
            <TextBox title={descriptionText} style={styles.descriptionText}/>
            <ConditionalDivider both={descriptionText && subDescriptionText}/>
            <TextBox title={subDescriptionText}
                     style={styles.subDescriptionText}/>
        </View>
    );
};

export default DescriptionContainer;

const TextBox = ({title, style}) => {
    if (title === undefined) {
        return null;
    }
    return <Text style={style}>{title}</Text>;
};

const ConditionalDivider = ({both}) => {
    if (!both) {
        return null;
    }
    return (
        <Divider
            hrStyles={styles.descriptionDivider}
            style={styles.descriptionDivider}
        />
    );
};
