import Text from '../../../../Text';
import {styles} from '../../Authentication.Style';
import Translate from '../../Lang';
import React from 'react';

const MatchPassword = ({password, reTypePassword, setMatch}) => {
    if (password === reTypePassword && reTypePassword !== '') {
        setMatch(true);
        return null;
    }
    setMatch(false);
    if (reTypePassword === '') {
        return null;
    }
    return (
        <Text style={styles.notMatchText}>
            {Translate('passwordsNotMatch')}
        </Text>
    );

};
export default MatchPassword;