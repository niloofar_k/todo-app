import {Icon, TouchableOpacity, View} from 'avijeh-ui-kit';
import {styles} from '../../Authentication.Style';
import Text from '../../../../Text';
import React from 'react';

const ExitOrBackButton = ({firstScreen}) => {
    if (!firstScreen) {
        return (
            <View style={styles.exitButton}>
                <Icon name={'times'} size={'tiny'}/>
            </View>
        );
    }
    return <Icon name={'arrow-right'} size={'small'} style={styles.backIcon}/>;
};

const CustomHeader = (props) => {
    const firstScreen = !!props.scene.descriptor.options.title;
    return (
        <View transparent style={styles.header}>
            <TouchableOpacity
                style={styles.touchableHeader}
                onPress={() => (firstScreen
                    ? props.navigation.goBack()
                    : null)}>
                <ExitOrBackButton firstScreen={firstScreen}/>
                <Text style={styles.headerText}>
                    {props.scene.descriptor.options.title}
                </Text>
            </TouchableOpacity>
        </View>
    );
};
export default CustomHeader;
