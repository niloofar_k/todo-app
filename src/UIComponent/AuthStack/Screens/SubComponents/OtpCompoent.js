import React, {useRef, useEffect} from 'react';
import {View} from 'avijeh-ui-kit';

import OptInputItem from './OptInputItem';

import {styles} from '../../Authentication.Style';

const OtpComponent = ({
    codeLength,
    disableSubmitButton,
    reset,
    shouldReset,
}) => {
    if (codeLength === undefined || codeLength <= 0) {
        return null;
    }
    const items = [];
    let combinedValue = '';
    const inputRef = [];
    const [data, setData] = React.useState(new Array(codeLength).fill(''));

    const getValue = (value, index) => {
        console.log('this is value', value, 'this is index', index);
        const dataCopy = [...data];
        dataCopy[index] = value;
        setData(dataCopy);
        console.log(dataCopy)
    };
    useEffect(() => {
        for (let i = 0; i < codeLength; i++) {
            if (data[i] !== '') {
                combinedValue += data[i];
            }
        }
        if (combinedValue.length === codeLength) {
            disableSubmitButton(false);
            console.log('combinedValue is: ', combinedValue);
        } else {
            disableSubmitButton(true);
        }
    }, [data]);

    for (let i = 0; i < codeLength; i++) {
        items.push(
            <OptInputItem
                key={`otpInput${i}`}
                index={i}
                codeLength={codeLength}
                inputRef={inputRef}
                getValue={getValue}
                reset={reset}
                shouldReset={shouldReset}
            />,
        );
        inputRef.push(useRef());
    }
    return <View style={styles.verificationContainer}>{items}</View>;
};
export default OtpComponent;
