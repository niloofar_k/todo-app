import React, {forwardRef, useState} from 'react';
import {TextInput} from 'avijeh-ui-kit';
import {styles} from '../../Authentication.Style';

const OptInputItem = ({
    index,
    codeLength,
    inputRef,
    getValue,
    reset,
    shouldReset,
}) => {
    const [inputValue, setValue] = useState('');
    if (reset) {
        inputRef.map((value, index) => {
            inputRef[index].current.clear();
            if (index === codeLength - 1) {
                shouldReset(false);
                inputRef[0].current.focus();
            }
        });
    }

    const handleChange = (inputValue) => {
        setValue(inputValue);
        getValue(inputValue, index);
        if (index !== codeLength - 1 && inputValue !== '') {
            inputRef[index + 1].current.focus();
        }
    };
    console.log(inputRef);
    return (
        <TextInputWithRef
            ref={inputRef[index]}
            callback={handleChange}
            onChangeText={handleChange}
            value={inputValue}
            maxLength={1}
            keyboardType={'numeric'}
            style={styles.verificationDigit}
            containerStyle={styles.verificationDigit}
        />
    );
};

export default OptInputItem;

const TextInputWithRef = forwardRef((props, ref) => {
    return <TextInput {...props} forwardedRef={ref}/>;
});