export const imgAddress = {
    RegisterImage: require('./RegisterImage/bitmap.jpg'),
    PasswordImage: require('./PasswordImage/bitmap.jpg'),
    CreatePasswordImage: require('./CreatePasswordImage/bitmap.jpg'),
    LoginImage: require('./LoginImage/bitmap.jpg'),
    VerifyImage: require('./VerifyImage/bitmap.jpg'),
};
