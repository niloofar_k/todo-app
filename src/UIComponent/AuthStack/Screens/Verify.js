import React, {useState} from 'react';
import {KeyboardAvoidingView} from 'react-native';
import {View, ScrollView, Button} from 'avijeh-ui-kit';
import {styles} from '../Authentication.Style';
import DescriptionContainer from './SubComponents/DescriptionContainer';
import FooterContainer from './SubComponents/FooterContainer';
import OtpComponent from './SubComponents/OtpCompoent';
import Translate from '../Lang';

const Verify = ({navigation}) => {
    const [status, setStatus] = useState(true);
    const [reset, setReset] = useState(false);

    return (
        <KeyboardAvoidingView style={styles.keyboardAvoid}>
            <ScrollView
                contentContainerStyle={styles.screenContainer}
                keyboardShouldPersistTaps={'always'}>
                <DescriptionContainer
                    imgFolder={'VerifyImage'}
                    descriptionText={Translate('codeSend')}
                    subDescriptionText={Translate('sixDigitCodeSent')}
                />
                <View style={styles.inputsContainer}>
                    <OtpComponent
                        codeLength={6}
                        disableSubmitButton={(status) => setStatus(status)}
                        reset={reset}
                        shouldReset={setReset}
                    />
                    <Button
                        style={styles.loginButton}
                        TextStyle={styles.buttonText}
                        title={Translate('enterYourAccount')}
                        onPress={() => navigation.navigate('CreatePassword')}
                        disabled={status}
                    />
                </View>
                <FooterContainer
                    title={Translate('sendAgain')}
                    buttonBackgroundColor={{backgroundColor: '#fff'}}
                    buttonTextStyle={{color: '#bbb'}}
                    buttonStyle={styles.buttonWithShadow}
                    footNote={Translate('newCodeAvailableIn')}
                    footerButtonAction={() => setReset(true)}
                />
            </ScrollView>
        </KeyboardAvoidingView>
    );
};

export default Verify;
