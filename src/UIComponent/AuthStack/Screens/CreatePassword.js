import React, {forwardRef, useRef, useState} from 'react';
import {KeyboardAvoidingView} from 'react-native';
import {View, Button} from 'avijeh-ui-kit';
import {styles} from '../Authentication.Style';
import {ScrollView, TextInput} from 'avijeh-ui-kit';
import DescriptionContainer from './SubComponents/DescriptionContainer';
import MatchPassword from './SubComponents/MatchPassword';
import {BarPasswordStrengthDisplay} from 'react-native-password-strength-meter';
import Translate from '../Lang';

const CreatePassword = () => {
    const [password, setPassword] = useState('');
    const [retypePassword, setRetypePassword] = useState('');
    const [match, setMatch] = useState(false);
    const secondInputRef = useRef();
    return (
        <KeyboardAvoidingView style={styles.keyboardAvoid}>
            <ScrollView
                contentContainerStyle={styles.passwordScreen}
                keyboardShouldPersistTaps={'always'}>
                <DescriptionContainer imgFolder={'CreatePasswordImage'}/>
                <View style={styles.inputsContainer}>
                    <TextInput
                        placeholder={Translate('password')}
                        secureTextEntry={true}
                        style={styles.mobileNumberText}
                        containerStyle={styles.mobileNumber}
                        placeholderTextColor={'#6d7278'}
                        value={password}
                        onChangeText={(value) => setPassword(value)}
                        callback={(value) => setPassword(value)}
                        //onSubmit={secondInputRef.current.focus()}
                    />
                    <BarPasswordStrengthDisplay
                        barStyle={styles.barStyle}
                        password={password}
                        labelVisible={false}
                        width={styles.passwordBar.width}
                        levels={customizedLevels}
                        minLength={1}
                    />
                    <TextInputWithRef
                        ref={secondInputRef}
                        placeholder={Translate('passwordAgain')}
                        secureTextEntry={true}
                        style={styles.mobileNumberText}
                        containerStyle={styles.mobileNumber}
                        placeholderTextColor={'#6d7278'}
                        value={retypePassword}
                        onChangeText={(value) => setRetypePassword(value)}
                        callback={(value) => setRetypePassword(value)}
                    />
                    <MatchPassword
                        password={password}
                        reTypePassword={retypePassword}
                        setMatch={setMatch}/>
                    <Button
                        style={[
                            styles.loginButton,
                            styles.confirmPasswordButton]}
                        TextStyle={styles.buttonText}
                        title={Translate('newPassword')}
                        disable={match}
                    />
                </View>
            </ScrollView>
        </KeyboardAvoidingView>);
};
const TextInputWithRef = forwardRef((props, ref) => {
    return <TextInput {...props} forwardedRef={ref}/>;
});
const customizedLevels = [
    {
        label: 'Pathetically weak',
        labelColor: '#ff2900',
        activeBarColor: '#ff2900',
    },
    {
        label: 'Extremely weak',
        labelColor: '#ff3e00',
        activeBarColor: '#ff3e00',
    },
    {
        label: 'Very weak',
        labelColor: '#ff5400',
        activeBarColor: '#ff5400',
    },
    {
        label: 'Weak',
        labelColor: '#ff6900',
        activeBarColor: '#ff6900',
    },
    {
        label: 'So-so',
        labelColor: '#f4d744',
        activeBarColor: '#f4d744',
    },
    {
        label: 'Average',
        labelColor: '#f3d331',
        activeBarColor: '#f3d331',
    },
    {
        label: 'Fair',
        labelColor: '#f2cf1f',
        activeBarColor: '#f2cf1f',
    },
    {
        label: 'Strong',
        labelColor: '#01b551',
        activeBarColor: '#01b551',
    },
    {
        label: 'Very strong',
        labelColor: '#01b551',
        activeBarColor: '#01b551',
    },
    {
        label: 'Unbelievably strong',
        labelColor: '#01b551',
        activeBarColor: '#01b551',
    },
];

export default CreatePassword;
