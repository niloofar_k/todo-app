import React from 'react';
import {KeyboardAvoidingView} from 'react-native';
import {View, Button, ScrollView, TextInput} from 'avijeh-ui-kit';
import {styles} from '../Authentication.Style';
import DescriptionContainer from './SubComponents/DescriptionContainer';
import FooterContainer from './SubComponents/FooterContainer';
import Translate from '../Lang';

const Login = ({navigation}) => {
    return (
        <KeyboardAvoidingView style={styles.keyboardAvoid}>
            <ScrollView
                contentContainerStyle={styles.screenContainer}
                keyboardShouldPersistTaps={'always'}>
                <DescriptionContainer
                    imgFolder={'LoginImage'}
                    descriptionText={Translate('letsStart')}
                    subDescriptionText={Translate('info')}
                />
                <View style={styles.inputsContainer}>
                    <TextInput
                        placeholder={Translate('enterMobileNumber')}
                        keyboardType={'phone-pad'}
                        style={styles.mobileNumberText}
                        containerStyle={styles.mobileNumber}
                        placeholderTextColor={'#6d7278'}
                    />
                    <Button
                        style={styles.loginButton}
                        TextStyle={styles.buttonText}
                        title={Translate('enterAccount')}
                        onPress={() => navigation.navigate('Password')}
                    />
                </View>
                <FooterContainer
                    title={Translate('register')}
                    footerButtonAction={() => navigation.navigate('Register')}
                />
            </ScrollView>
        </KeyboardAvoidingView>
    );
};
export default Login;
