import React from 'react';
import {KeyboardAvoidingView} from 'react-native';
import {View, Button, ScrollView, TextInput} from 'avijeh-ui-kit';
import {styles} from '../Authentication.Style';
import DescriptionContainer from './SubComponents/DescriptionContainer';
import FooterContainer from './SubComponents/FooterContainer';
import Translate from '../Lang';

const Password = ({navigation}) => {
    return (
        <KeyboardAvoidingView style={styles.keyboardAvoid}>
            <ScrollView
                contentContainerStyle={styles.screenContainer}
                keyboardShouldPersistTaps={'always'}>
                <DescriptionContainer
                    imgFolder={'PasswordImage'}
                    descriptionText={Translate('enterPassword')}
                    subDescriptionText={Translate('passwordDidForget')}
                />
                <View style={styles.inputsContainer}>
                    <TextInput
                        placeholder={Translate('password')}
                        secureTextEntry={true}
                        style={styles.mobileNumberText}
                        containerStyle={styles.mobileNumber}
                        placeholderTextColor={'#6d7278'}
                    />
                    <Button
                        style={styles.loginButton}
                        TextStyle={styles.buttonText}
                        title={Translate('enterYourAccount')}
                    />
                </View>
                <FooterContainer
                    title={Translate('forgetPassword')}
                    buttonBackgroundColor={{backgroundColor: '#fff'}}
                    buttonTextStyle={{color: '#bbb'}}
                    buttonStyle={styles.buttonWithShadow}
                    footNote={Translate('enterWithOTP')}
                    footNoteStyle={{color: '#f18d22'}}
                    touchableText={true}
                    footerButtonAction={() => navigation.navigate('Verify')}
                />
            </ScrollView>
        </KeyboardAvoidingView>
    );
};

export default Password;
