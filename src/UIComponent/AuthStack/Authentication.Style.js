import {StyleSheet, Dimensions} from 'react-native';

const {width, height} = Dimensions.get('window');

const noScrollHeight = height - 80;
const bold = 'IRANYekanMobileBold';
const regular = 'IRANYekanMobileRegular';

export const styles = StyleSheet.create({
    //Todo: manage font family of input and button

    //  All Screens Style-----------------------------

    //general styles-----------------------------
    keyboardAvoid: {
        flex: 1,
    },
    screenContainer: {
        flexGrow: 1,
        justifyContent: 'flex-end',
        maxHeight: noScrollHeight,
        backgroundColor: '#fff',
    },
    //description container styles-------------------
    descriptionContainer: {
        marginHorizontal: 16,
        marginBottom: 16,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexShrink: 1,
    },
    imageSize: {
        resizeMode: 'center',
        flexShrink: 1,
    },
    descriptionDivider: {
        backgroundColor: '#f18d22',
        width: 20,
        height: 2,
        marginVertical: 5,
    },
    descriptionText: {
        width: 255,
        fontSize: 20,
        fontFamily: bold,
        textAlign: 'center',
    },
    subDescriptionText: {
        width: 255,
        fontSize: 18,
        lineHeight: 25,
        color: '#bbb',
        textAlign: 'center',
    },
    //Footer Container styles------------------------
    footerContainer: {
        paddingHorizontal: 16,
        paddingVertical: 24,
        backgroundColor: '#f7f7f7',
        alignItems: 'center',
        justifyContent: 'space-around',
    },
    footerButton: {
        marginBottom: 24,
        borderRadius: 3,
        backgroundColor: '#ee8258',
        borderWidth: 0,
        width: '100%',
        height: 48,
    },
    buttonWithShadow: {
        backgroundColor: '#fff',
        color: '#bbb',
        shadowColor: 'rgba(0,0,0,0.25)',
        shadowOffset: {
            width: 0,
            height: -10,
        },
        shadowOpacity: 0.1,
        shadowRadius: 22,
        elevation: 2.5,
    },
    buttonText: {
        color: '#fff',
        fontSize: 16,
        fontFamily: regular,
    },
    footerText: {
        color: '#bbb',
        fontSize: 14,
        fontFamily: bold,
        letterSpacing: 0.62,
    },
    avijeh: {
        color: 'rgba(242, 92, 98, 0.77)',
        fontSize: 14,
        letterSpacing: 0.62,
        fontFamily: bold,
    },
    touchable: {
        height: 25,
        width: 134,
        position: 'absolute',
        bottom: 24,
    },

    //Inputs Container styles---------------------------
    inputsContainer: {
        alignItems: 'center',
        justifyContent: 'space-around',
        marginHorizontal: 16,
        marginVertical: 8,
    },
    mobileNumber: {
        height: 48,
        width: '100%',
        marginVertical: 8,
    },
    mobileNumberText: {
        backgroundColor: 'rgba(255, 255, 255, 0)',
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.25)',
        borderRadius: 3,
        height: 48,
        width: '100%',
        textAlign: 'center',
        marginVertical: 'auto',
        fontSize: 14,
        fontFamily: regular,
    },
    loginButton: {
        backgroundColor: '#f18d22',
        borderColor: '#f18d22',
        height: 48,
        width: '100%',
        textAlign: 'center',
        marginVertical: 8,
        marginBottom: 18,
    },
    verificationContainer: {
        height: 48,
        width: '100%',
        marginVertical: 8,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    verificationDigit: {
        height: 48,
        width: 48,
        textAlign: 'center',
        borderWidth: 1,
        borderColor: 'rgba(0, 0, 0, 0.2)',
        borderRadius: 3,
        backgroundColor: '#fff',
    },
    //Rules Screen Styles------------------------------
    rulesScreen: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    rulesContainer: {
        paddingHorizontal: 16,
    },
    rulesText: {
        lineHeight: 24,
        fontSize: 14,
        paddingVertical: 8,
    },
    rulesButton: {
        backgroundColor: '#aeaeb3',
        borderColor: '#aeaeb3',
        height: 50,
        width: width - 32,
        marginVertical: 24,
    },
    // Confirm password styles------------------------------------
    passwordBar: {
        width: width - 32,
    },
    passwordScreen: {
        justifyContent: 'space-around',
    },
    confirmPasswordButton: {
        marginVertical: 24,
    },
    barStyle: {
        marginHorizontal: 0,
    },
    notMatchText: {
        color: '#f00',
        fontSize: 14,
        width: width - 32,
    },

    // Register styles-----------------------------------------
    registerContainer: {
        width: '100%',
        height: 24,
        marginVertical: 8,
    },
    introduceCheckBox: {
        borderColor: '#f18d22',
        height: 24,
        width: 24,
    },
    introducer: {
        color: '#6d7278',
        fontSize: 14,
        marginHorizontal: 4,
    },
    agreeText: {
        flexShrink: 1,
        lineHeight: 26,
        paddingHorizontal: 1,
    },
    readableRules: {
        color: '#f18d22',
    },
    agreeContainer: {
        width: '100%',
        marginVertical: 8,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    switchStyle: {
        height: 31,
        width: 51,
    },
    // header style---------------------------------
    exitButton: {
        height: 20,
        width: 20,
        borderWidth: 2,
        borderRadius: 3,
        borderColor: '#323232',
        alignItems: 'center',
        justifyContent: 'center',
    },
    // header Style----------------------------------
    header: {
        margin: 'auto',
        height: 55,
        backgroundColor: '#fff',
        shadowColor: 'rgba(0,0,0,0.25)',
        shadowOffset: {
            width: 0,
            height: -10,
        },
        shadowOpacity: 0.1,
        shadowRadius: 22,
        elevation: 2.5,
    },
    touchableHeader: {
        height: 55,
        flexDirection: 'row-reverse',
        justifyContent: 'flex-start',
        alignItems: 'center',
        marginHorizontal: 16,
    },
    headerText: {
        fontFamily: regular,
        marginHorizontal: 8,
        fontSize: 18,
        color: '#333',
        lineHeight: 18,
    },
    backIcon: {
        color: '#323232',
    },
});
