import React from 'react';
import {TemplateProvider, Themes} from 'avijeh-ui-kit';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import CreatePassword from './Screens/CreatePassword';
import Login from './Screens/Login';
import Password from './Screens/Password';
import Register from './Screens/Register';
import Rules from './Screens/Rules';
import Verify from './Screens/Verify';
import Translate from './Lang';
import CustomHeader from './Screens/SubComponents/AuthHeader';

const Stack = createStackNavigator();

const Authentication = () => {
    return (
        <TemplateProvider theme={Themes.light}>
            <NavigationContainer>
                <Stack.Navigator screenOptions={{footerShown: false}}>
                    <Stack.Screen
                        name="Login"
                        component={Login}
                        options={{
                            title: '',
                            header: CustomHeader,
                            headerLeft: null,
                        }}
                    />
                    <Stack.Screen
                        name="Rules"
                        component={Rules}
                        options={{
                            title: Translate('rules'),
                            headerLeft: null,
                            header: CustomHeader,
                        }}
                    />
                    <Stack.Screen
                        name="Password"
                        component={Password}
                        options={{
                            title: Translate('password'),
                            headerLeft: null,
                            header: CustomHeader,
                        }}
                    />
                    <Stack.Screen
                        name="Verify"
                        component={Verify}
                        options={{
                            title: Translate('verify'),
                            headerLeft: null,
                            header: CustomHeader,
                        }}
                    />
                    <Stack.Screen
                        name="Register"
                        component={Register}
                        options={{
                            title: '',
                            headerLeft: null,
                            header: CustomHeader,
                        }}
                    />
                    <Stack.Screen
                        name="CreatePassword"
                        component={CreatePassword}
                        options={{
                            title: Translate('confirmPassword'),
                            headerLeft: null,
                            header: CustomHeader,
                        }}
                    />
                </Stack.Navigator>
            </NavigationContainer>
        </TemplateProvider>
    );
};

export default Authentication;
