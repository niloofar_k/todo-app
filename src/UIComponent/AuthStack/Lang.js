import React from 'react';

const items = {
    home: 'خانه',
    enterYourAccount: ' ورود به حساب کاربری',
    sendAgain: 'دوباره بفرست!',
    newCodeAvailableIn: 'امکان ارسال دوباره در (۴۵) ثانیه دیگر',
    enterAccount: 'ورود به حساب',
    newPassword: 'ورود و ثبت رمز عبور جدید',
    password: 'رمز عبور',
    passwordAgain: ' رمز عبور را مجددا وارد کنید',
    enterMobileNumber: 'شماره همراه خود را وارد کنید',
    forgetPassword: 'فراموشی رمز',
    createAccount: 'ساخت حساب کاربری',
    enterWithOTP: 'ورود با رمز یکبار مصرف',
    rules: 'شرایط و قوانین',
    verify: 'تایید هویت',
    confirmPassword: 'ثبت رمز عبور',
    letsStart: 'خوب بیا شروع کنیم',
    info: 'بیشتر از ۱۰۰۰ نقد و برسی در صنعت خودرو',
    register: 'ثبت نام و ساخت حساب کاربری',
    codeSend: 'یک رمز براتون فرستادیم',
    sixDigitCodeSent:
        'یک پیامک حاوی کد برای شما ارسال کردیم کد ۶ رقمی را وارد کنید',
    enterPassword: 'رمز رو وارد کن !',
    passwordDidForget:
        'اگه یادتون نیست که رمزتون چیه از قسمت فراموشی رمز استفاده کنید',
    passwordsNotMatch:
        'رمز عبورهای وارد شده یکسان نیست!',
};

const itemsEN = {};

export default function Translate(key, lang = 'fa') {
    switch (lang) {
        case 'en':
            return itemsEN[key] || key;
        default:
            return items[key] || key;
    }
}
