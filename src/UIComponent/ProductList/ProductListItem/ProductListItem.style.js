import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    container: {
        marginHorizontal: 16,
        marginVertical: 4,
        height: 205,
        borderWidth: 1,
        padding: 8,
        borderRadius: 3,
        borderColor: '#e6e6e6',
    },
    imageContainer: {
        width: 120,
        backgroundColor: '#f5f5f5',
        borderRadius: 3,
        justifyContent: 'center',
        alignItems: 'center',
        paddingVertical: 5,
    },
    mainImage: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain',
    },
    logo: {
        position: 'absolute',
        bottom: 8,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        resizeMode: 'contain',
        width: '88%',
        height: '15%',
        borderRadius: 5,
    },
    infoContainer: {
        flex: 1,
        paddingRight: 16,
        paddingLeft: 8,
    },
    Ad: {
        position: 'absolute',
        backgroundColor: '#ff8800',
        height: 20,
        width: 30,
        top: -3,
        right: -3,
        borderRadius: 3,
        alignItems: 'center',
        justifyContent: 'center',
    },
    AdText: {
        color: '#fff',
    },
    details: {
        flex: 1,
        alignItems: 'flex-end',
        width: '100%',
    },
    title: {
        flex: 3,
        width: '100%',
        fontSize: 17,
        lineHeight: 24,
        color: '#4a4a4a',
    },
    countryAndYear: {
        flex: 1,
    },
    yearBox: {
        flex: 1,
        alignItems: 'center',
    },
    year: {
        backgroundColor: '#e6e6e6',
        height: 22,
        color: '#666',
        paddingHorizontal: 5,
    },
    cloudIcon: {
        color: '#666',
    },
    flagContainer: {
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
    },
    flag: {
        resizeMode: 'cover',
        height: 22,
        width: 22,
        borderRadius: 22,
        marginRight: 8,
    },
    hr: {
        // marginLeft: 16,
        marginTop: 17,
    },
    priceBox: {
        height: 46,
        width: '100%',
        alignItems: 'center',

    },

    priceContainer: {
        alignItems: 'flex-end',
        flex: 1,
    },
    price: {
        flex: 1,
        fontSize: 24,
        color: '#303030',
    },
    toman: {
        fontSize: 11,
        paddingTop: 4,
        margin: 6,
        color: '#303030',
    },
    discountPercentContainer: {
        height: 24,
        marginHorizontal: 10,
    },
    discountPrice: {
        backgroundColor: '#e83f54',
        color: '#fff',
        fontSize: 16,
        paddingHorizontal: 4,
        textAlign: 'center',
    },
    beforeDiscountPrice: {
        borderTopColor: '#e83f54',
        borderTopWidth: 24,
        borderLeftWidth: 0,
        borderRightWidth: 3,
        borderRightColor: 'transparent',
        borderLeftColor: 'transparent',
    },
    afterDiscountPrice: {
        borderBottomColor: '#e83f54',
        borderBottomWidth: 22,
        borderLeftWidth: 3,
        borderRightWidth: 0,
        borderRightColor: 'transparent',
        borderLeftColor: 'transparent',
    },
    yearContainer: {
        backgroundColor: '#e6e6e6',
        marginHorizontal: 4,
    },
    beforeYearContainer: {
        borderTopColor: '#e6e6e6',
        borderTopWidth: 22,
        borderLeftWidth: 0,
        borderRightWidth: 3,
        borderRightColor: 'transparent',
        borderLeftColor: 'transparent',
    },
    afterYearContainer: {
        borderBottomColor: '#e6e6e6',
        borderBottomWidth: 22,
        borderLeftWidth: 3,
        borderRightWidth: 0,
        borderRightColor: 'transparent',
        borderLeftColor: 'transparent',
    },

});
