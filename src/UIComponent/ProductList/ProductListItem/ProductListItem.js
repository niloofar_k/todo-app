import React from 'react';
import {Image} from 'react-native';
import {styles} from './ProductListItem.style';
import {Themes, Divider, TemplateProvider, View, Row, Icon} from 'avijeh-ui-kit';
import Text from '../../../myText';

const ProductListItem = (props) => {

    return (<TemplateProvider theme={Themes.light}>
            <Row style={styles.container}>
                <View transparent style={styles.imageContainer}>
                    <Row transparent style={styles.Ad}>
                        <Text transparent style={styles.AdText}>
                            {props.ad}
                        </Text>
                    </Row>
                    <Image
                        style={styles.mainImage}
                        source={{
                            uri: 'https://m.hankooktire.com/content/dam/htg-mobile/common/images/tire/passenger-cars-tires/h735/hankook-tires-kinergy-h735-left-01.png',
                        }}
                    />
                    <Image style={styles.logo}
                           source={{uri: 'https://www.marshystyres.com.au/wp-content/uploads/2018/11/hankook_logo.png'}}
                    />
                </View>

                <View transparent style={styles.infoContainer}>

                    <View transparent style={styles.details}>
                        <Text transparent style={styles.title}>
                            {props.title}
                        </Text>
                        <Row transparent style={styles.countryAndYear}>
                            <Row transparent style={styles.yearBox}>

                                <Icon solid name={'cloud'} size={'small'}
                                      style={styles.cloudIcon}/>
                                <Row transparent style={styles.yearContainer}>
                                    <Row style={styles.beforeYearContainer}/>
                                    <Text style={styles.year}>
                                        {props.year}
                                    </Text>
                                    <View style={styles.afterYearContainer}/>
                                </Row>
                            </Row>
                            <Row transparent style={styles.flagContainer}>
                                <Image
                                    style={styles.flag}
                                    source={{
                                        uri: 'https://cdn.countryflags.com/thumbs/germany/flag-400.png',
                                    }}
                                />
                                <Image
                                    style={styles.flag}
                                    source={{uri: 'https://cdn.countryflags.com/thumbs/hungary/flag-800.png'}}/>

                            </Row>

                        </Row>
                    </View>
                    <Divider style={styles.hr}/>

                    <Row transparent style={styles.priceBox}>
                        <Row transparent style={styles.discountPercentContainer}>
                            <Row style={styles.beforeDiscountPrice}/>
                            <Text style={styles.discountPrice}>
                                {props.discountPercentage}
                            </Text>
                            <View style={styles.afterDiscountPrice}/>
                        </Row>
                        <Row transparent style={styles.priceContainer}>
                            <Text style={styles.price}>
                                {props.price}
                            </Text>
                            <Text style={styles.toman}>
                                {props.toman}
                            </Text>

                        </Row>

                    </Row>
                </View>

            </Row>
        </TemplateProvider>
    );

};

export default ProductListItem;


ProductListItem.defaultProps = {
    year: '۲۰۱۸',
    title: 'لاستیک خودرو گلدستون مدل ۲۰۰۰-۲۰۲۰ سایز R۱۱/۱۸۵/۶۵ دو حلقه مناسب برای انواع رینگ',
    price: '۴۹۳,۰۰۰',
    toman: 'تومان',
    ad: 'Ad',
    discountPercentage: ' ٪ ۲۲',
};

