import React from 'react';
import {TouchableOpacity} from 'react-native';
import {styles} from './ProductList.style';
import {Themes, Divider, ScrollView, TemplateProvider, View, Row, Icon, Button} from 'avijeh-ui-kit';
import ProductListItem from '/home/user05/WebstormProjects/untitled/MyTestApp/src/UIComponent/ProductList/ProductListItem/ProductListItem';
import Text from '../../myText';

//todo: I couldn't use Button due to its default width, instead I used TouchableOpacity
//todo: Exact cloud Icon wasnot found
const ProductList = (props) => {
    return (
        <TemplateProvider theme={Themes.light}>
            <Row style={styles.filterContainer}>
                <TouchableOpacity style={styles.mainFilter}>
                    <Icon name={'list'} size={'tiny'} style={styles.filterIcon}/>
                    <Text style={styles.mainFilterText}>
                        {props.filter}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.filter1}>
                    <Text style={styles.filter1Text} numberOfLines={1}>
                        {props.tire}
                    </Text>
                    <Icon name={'times'} size={'tiny'} style={styles.timesIcon}/>
                </TouchableOpacity>
            </Row>
            <ScrollView style={styles.container}>
                <ProductListItem/>
                <ProductListItem/>
                <ProductListItem/>
            </ScrollView>
        </TemplateProvider>
    );
};
export default ProductList;

ProductList.defaultProps = {
    filter: 'فیلتر',
    tire: 'لاستیک ۲۰۵',
};
