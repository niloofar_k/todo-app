import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
    container: {
        backgroundColor: '#f5f5f5',
    },
    filterContainer: {
        margin: 16,
    },
    mainFilter: {
        flexDirection: 'row-reverse',
        backgroundColor: '#f18d22',
        marginLeft: 10,
        borderColor: '#f18d22',
        borderWidth: 1,
        borderRadius: 3,
        alignItems: 'center',
        padding: 16,
        height: 40,
    },
    mainFilterText: {
        color: '#fff',
        fontSize: 15,

    },
    filter1: {
        flexDirection: 'row-reverse',
        backgroundColor: '#fff',
        marginLeft: 10,
        borderColor: '#e6e6e6',
        borderWidth: 1,
        borderRadius: 3,
        padding: 16,
        justifyContent: 'space-around',
        alignItems: 'center',
        height: 40,

    },
    filterIcon: {
        color: '#fff',
        fontSize: 15,
        marginHorizontal: 4,
    },
    filter1Text: {
        color: '#666',
        fontSize: 15,
        padding: 6,

    },
    timesIcon: {
        color: '#666',
        fontSize: 15,
        marginHorizontal: 4,

    },
});

