import React from 'react';
import {Image, ScrollView} from 'react-native';
import {Divider, View, TemplateProvider, Themes, Row, Button, Icon} from 'avijeh-ui-kit';
import Swiper from 'react-native-web-swiper';
import Text from '../../myText';
import {styles} from './ProductItem.style';

const ProductItem = (props) => {
    return (
        <TemplateProvider theme={Themes.light}>
            <ScrollView style={styles.container}>
                <Row transparent style={styles.ratingContainer}>
                    <Text style={styles.suggested}>
                        پیشنهاد شده توسط {props.suggestedBy} نفر
                    </Text>
                    <View transparent style={styles.starRating}>
                        <Text>
                            <Icon name={'star'} size={'tiny'}/>
                            <Icon name={'star'} size={'tiny'}/>
                            <Icon name={'star'} size={'tiny'}/>
                            <Icon name={'star'} size={'tiny'}/>
                            <Icon name={'star'} size={'tiny'}/>
                        </Text>
                    </View>
                    <Row transparent style={[styles.parallContainer, styles.rateContainer]}>
                        <Row transparent style={[styles.beforeParallContainer, styles.beforeRate]}/>
                        <Text style={[styles.parallText, styles.rate]}>
                            {props.rate}
                        </Text>
                        <Row transparent style={[styles.afterParallContainer, styles.afterRate]}/>
                    </Row>
                </Row>

                <View transparent style={styles.imageContainer}>
                    <Swiper>
                        <Image style={styles.mainImage}
                               source={{uri: 'https://oilbest.ir/wp-content/uploads/2020/02/NIL-367-053-600x600.jpg'}}
                        />
                    </Swiper>

                    <Image transparent style={styles.logo}
                           source={{uri: 'https://logos-download.com/wp-content/uploads/2016/02/Castrol_logo_2D_transparent-700x178.png'}}
                    />
                    <View transparent style={styles.buttonBox}>
                        <Button style={styles.notifyMe}>
                            <Icon name={'bell'} size={'small'} style={styles.bellIcon}/>
                        </Button>
                        <Button style={styles.priceHistory}>
                            <Icon name={'line-chart'} size={'small'} style={styles.bellIcon}/>
                        </Button>

                    </View>
                    <Row transparent style={styles.thumbsBox}>
                        <Button style={styles.like}>
                            <Text style={styles.likeText}>
                                {props.like}
                            </Text>
                            <Icon name={'thumbs-up'} size={'tiny'} style={styles.likeIcone}/>
                        </Button>
                        <Button style={styles.disLike}>
                            <Text style={styles.dislikeText}>
                                {props.dislike}
                            </Text>
                            <Icon name={'thumbs-down'} size={'tiny'} style={styles.disLikeIcone}/>
                        </Button>
                    </Row>
                </View>
                <Row transparent style={styles.nameContainer}>
                    <View transparent style={styles.vLine}/>
                    <View transparent style={styles.nameBox}>
                        <Text style={styles.titleBox}>
                            {props.title}
                        </Text>

                        <Text style={styles.subtitleBox} numberOfLines={1}>
                            {props.subtitle}
                        </Text>
                    </View>
                </Row>
                <Divider/>
                <Row transparent style={styles.detailsContainer}>
                    <Row transparent style={styles.rightDetailsContainer}>
                        <Icon name={'sun'} size={'small'} style={styles.sunIcon}/>

                        <Row transparent style={styles.parallContainer}>
                            <Row transparent style={[styles.beforeParallContainer, styles.beforeBrand]}/>
                            <Text style={[styles.parallText, styles.brand]}>
                                {props.brand}
                            </Text>
                            <Row transparent style={[styles.afterParallContainer, styles.afterBrand]}/>
                        </Row>

                        <Row transparent style={styles.parallContainer}>
                            <Row transparent style={[styles.beforeParallContainer, styles.beforeYear]}/>
                            <Text style={[styles.parallText, styles.year]}>
                                {props.year}
                            </Text>
                            <Row transparent style={[styles.afterParallContainer, styles.afterYear]}/>
                        </Row>
                    </Row>
                    <Row style={styles.leftDetailsContainer}>
                        <Button style={styles.productType}>
                            <Text style={styles.productTypeText}>
                                {props.productType}
                            </Text>
                        </Button>
                    </Row>

                </Row>
                <Row transparent style={styles.suitableContainer}>
                    <Text style={styles.suitableFor}>
                        {props.suitableText}
                    </Text>
                    <ScrollView
                        inverted={true}
                        contentContainerStyle={styles.suitableList}
                        horizontal={true}
                        showsHorizontalScrollIndicator={false}>
                        {[1, 2, 3, 4, 5, 6, 7].map(() => {
                            return (
                                <View transparent style={styles.suitableListItem}>
                                    <Text style={styles.suitableListItemText}>
                                        {props.suitableItem}
                                    </Text>
                                </View>
                            );
                        })}


                    </ScrollView>
                </Row>
                <View transparent style={styles.bestSeller}>
                    <Row transparent style={styles.bestSellerNameContainer}>
                        <View transparent style={styles.logoHolder}>
                            <Image style={styles.bestSellerLogo}
                                //   source={{uri: 'https://thumbs.dreamstime.com/z/gear-drop-logo-vector-design-inspiration-oil-gas-gear-drop-logo-vector-design-139801692.jpg'}}/>
                                   source={{
                                       uri: 'https://m.hankooktire.com/content/dam/htg-mobile/common/images/tire/passenger-cars-tires/h735/hankook-tires-kinergy-h735-left-01.png',
                                   }}/>
                            <Image style={styles.checkSign}
                                   source={{uri: 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAPEBUPEBAQEhAVEBUWFw8VFhcQEhUWFRUWFhYVFRUYHSggGBolGxUVITEhJSkrLi4uFx8zODMsNygtLisBCgoKDg0OGxAQGy0lICUtLS0uLS0tLS0tLy0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAOAA4AMBEQACEQEDEQH/xAAbAAEAAgMBAQAAAAAAAAAAAAAABAYDBQcCAf/EADgQAAIBAgMHAgQFAwMFAAAAAAABAgMRBAUhBhITMUFRYTJxFCKBkSNCocHRUmKxM7LwBxYkcqL/xAAbAQEAAgMBAQAAAAAAAAAAAAAAAQYDBAUCB//EACsRAAICAgICAgICAgIDAQAAAAABAgMEEQUSITETQTJhIlGBkUJxFCNDFf/aAAwDAQACEQMRAD8A7iAACFV5v3APIBKw/p+oBlAI+K6AGAAy4bn9ACUAVjbjOXh6XDhK1SfXqkb+BjfLPb9I5XKZjor1H2yjZZtBXozUt+U49Yyd00dm7BqnDSWmcDH5K6qe5PaOh5Pm1LFRUqctesH6l9CvX486XqSLVjZdeRHcGNps6WDp3dnOXpj57vwZMXGd0tfRizsxY8N/ZyvFYidWbqTd5Sd2yz1wUIqKKbZbKyTnL2yx7G53wp8Co/kk/lfZnM5HE7L5I+zscTndH8UvTL+cAtJCzbaGjhIfNLeqW0prV/XsbWPiTufj0aWVnV0Ly/P9GoyfbdVaqp1YKEZOyknyfk27+McIdovejQxuZjZZ0ktbLTiea9jlHcMQBnwvUAkAGLEen6gEUA9U+a9wCaAAAAACFV5v3APIBKw/p+oBlAI+K6AFK2n2mnRqcGlZONryeuvY62FgRtj3mcLkeTnTP44DJduFe2Ihb++P7o938U0t1v8AweMbm03q1f5Lhh80o1Ib8KkXFK715HKlTOMtNHZhkVzj2izlW0mZvE4iU/yptRXhFnw6PiqS+yn5+S77m/o1ZtmiSMDjalCaqU5bsl/yzMNtMbY9ZIzU3zpl2iz3meY1MTN1Ksrvt0XsRTRCmPWJ6yMmd8u0yIZ29muE7armeWtkp6e0WT/u+twVSSSmlZ1Otv5OZ/8AmQ+Ts34Ow+Zs+Lol5/sr1WrKb3pNtvq9TpQio+EjkzslN7k9nyEmmmuadyZLa0RGXVpo61lONVehTqddxJ+60ZUsiv47HEvWJaraoyJZgNk9YbEQUt3ejftdXPXWWt6PPeO9bJp5PRixHp+oBFAPVPmvcAmgAAhcR92AOI+7AJMIJpNpXsAfeGuyAMFV2dlouyAPHEfdgGWjqm5a27gHIM9r8TEVZ81vu3smW3Fh0qSKJm2d75P9kA2DVPUKko8m1fs7EdIy+j1Gcl6Z5JX7PIJIAAAAAAAAIAH0SWLZ7aZ4SlKm4b93eN+S7nNy8D5pqSejrYPJvHrcWt/0Ysw2qxNbRSUI9oq36nurjqYe1s8XctfZ6ekadYme9vb0t5O97u5tuqDXXRoq6fbtt7Os5TipVKFObbu4K5U7o9bGi9Y83OqLZOott2eq7GIzGfhrsgD5OCSbSV7AEbiPuwBxH3YB5AABMpcl7AHsAp+2G0csNUVOmk5WTbeqsdLBwlenKT8HI5HkXjtRivJo6G21ZeunCXtobs+Kg/xZzoc5YvyRsY7dU9yS4clJxdtbq9jAuJmpe/BsvnIOPp7KLKV233Z3IrSSK1J7bZ8PX2QCNAEgAgAAAAAAAAAEEgAABEkHT9k572Ep37NfqVTOWr5F342TeNHZvMP6voahvkq4B8q+l+wBCAABK4EfIA4EfIBilVadlyQB848gDne36fxKk+sF+hYeKa+Jr9lU5tf+5P8ARWDqnFAIAJAIAAAAAAAAAAAAAAAAAABBJvMt2mrYemqUFFpdzQv4+Fs+zZ1MflbKa1BIyT2wxbd4yjHTojyuLpR6lzGQzZbO7WYiVaNOq9+M2ly1Rr5nH1wr7Q+jb4/lLZ2qFnnZeo1W3Z8mcMshl4EfIA4EfIBlAABBqu0n7jRG0YJ4qnHnOC+qPark/o8u2C9tFI28rwnOnuSUvld7O/U7nFxcYy2itc3OMpx6sqp1zhAEAAAAAAAAAAAAAAAAAAAAAAEggAA3GyVDfxdPw977amlyElGhnR4qHbJX6Oo0+a9yrl0JoAAI/wAR4APksS7cuhKIfhHJc4zKtUrTcpyXzPS9kvFi04uPCNa0ik5mVbK17bRrpVG+bb+ptdUvSNNzk/bPLZJ5bbBIAIAAAAAAAAAAAAAAAAAAAAAAAABDJLLsLUpRrt1Jxh8lk3pqzmcnGcq0oo7HDShG1uR0ak4Nb0ZqVtdGmV5xa9otcZxl6Z7+I8EHofEeADAAADm+2eB4WJbS+WcVJfuWXjbe9Wv6Kdy9Px37/s0B0PBy2AASQAAAAAAAAAAAAAAAAAAAAQASSfYxb0Sv4R5ckvZ6UW3pEqvl1WnBVJwlGLdk3pf6GKGRCcusXszTxra49pLSIhnNcEAy0sROHpnJezaPDrjL2jJC6yH4tlg2e2irqrGE5ucZSSs9Wc7Mwa+jlFaaOtgcld8ihLymdCK8Wsy/DvwAeKsFBXlKKXduxKTfo8ykkvJR9ucxw9aMI0571SDtdcre52+Mptg22vBXOYvpsSUX5RTjtFfYBAAAAAAAAAAAAAAAAAAAAAASvyIb0Sk34RY8k2Uq1rSqXp0//p+yOZkclGvxHyzsYnEzt/lPwi11MLg8BS391XXWWspPsjlKd+TPR23Vj4dfbRQc5zWpip78nZflj0SO/jY0aI6XsrGZmSyJbfr+jXm0aYBAIe9Elp2Dyt1azqtfLBde75HK5S/rBQXtnb4bGcrHY/SOh/DvuivlqNRthnksJTW4vnndJvVLybuFjK+en6RzuRzHj17j7ZzfHZrXru9SpKXi9l9ixVY1Vf4oql2Zdb+TIRmNYEkAAAAAAAAAAAAAAAAAAAAEAkYDBVK81TpxcpP/AJcx22xqj2kZ6KJ3S6wXk6BkuytPDWlUtOra/wDavZFeys+dviPhFqwuMrpW5eZG3xuLhQg6k3aK/XwjTrrlZLSOhbbGqPaXo5jnea1MVUcpP5fyx6JFnxcWNMdL2UzMzZZE9v19GuNs0gQQCQERvySdY2MpUoYWMYSjJvWVud30ZVs2UpWtyRdeNjCNCUWb40zoGg2ryyOLoNJ/iRu4/ujbw7/hs39GhyGKr6mvtHKqkHFtNWaeqLTGSktopcouLaf0eT0eAAAACQCAAASAQAAAAAAAAACCTYZRlFTFTUYLS+s+iRrZOTGiO37NvEw7MiWo+v7OoZLldDCQ3YW3raztqytX5E7pbkXDGxYUR6xRJxtaMYuo2lGK1b0MMYuT0jYnNQjtvwcu2izqWKqPVqmvTH92WfDxFVDb9sp3IZ0siel+KNObpzQSQAAACPokkYPG1KMt6nOUX4djFZVCxakjNVfZU9wZcsl255QxEfHEX7o5GRxev5V/6O9ic1v+Nv8AstpxiwlC26y+EJqrFpOfOHW/c73F3ykuj+ir8zjxjJTX2VU7BwQQSSsNl9aot6nTlJd0rmKd9cHqT0Z68a2xbitn2rl1eOsqU19CI5NT8KR6liXR8uLIsotc1Yyp9vRrtNez5YnR5ARIJIAAAAAAAABJtcgyOpjKm7HSC9U+iX8mnl5caY+fZvYWFPJl49HS8HgKeHjw6aslbXq3bmys22ysl2kXGiiNMVGJmbMZl3ooG1+f8aXApv8ADi9X/U/4LDx+H0XyT9lW5XkPkfxw9L2Vg6iRxAACSAAAAACNEknLsHOvUjTgrtv6Jd2Yr741wbkZ8eiV01GJeM+2qhRvCjadT+r8qODi8fKx7n4RZs3lI1LrX5ZRMXip1ZOc5NyfU79VUa46iir3XTtl2kzCZTCASbDKM5q4WV4P5esHyZqZGJC5eff9m5iZtmO/4+v6OgZNn1HFq2kZ9YP9u5X8jEspf6/stWLnVZC19/0bCrk+HrJ8WlF+bWZihkWQ/FmezFqs/KJX8y2EpyvKhU3X/TLVfc6FPKyj4mtnKv4SEvNb0VDM8kr4Z/iQdv6lqvudanLrt9M4eRg3UfkvBrrGyaYBAJAAAABJs8hyeeLq7kdIr1S6JGplZSojv7N3Bw5ZM9L0dWyzA0sNTVOmkkuvVvuysW2ysl2kXKiiNMFGJ6rL5mYzMVHbPO+FH4em/nkvma6LsdTjsTvLvL0cTls744/HD2yhMsOvoquwPoAAEkAAAAA9UqbnJRirybsl5PE5KMW2eoRcmoo6hszs8sLTu7OrJXk+39qKzmZbul+i5cfhRx4bftnMa1KUJOMk1JPVPmWaDjJbiU+yEoyal7MZ6PAJIBBIAPVOpKL3otprqtGRKKktNHqM3F7iy5ZHti7KnidbaKp1+pxcvjf+Vf8AosODy/qF3+y4UqkZpSi04vk1qjjNOL0ywRkpLaM1OnGd4ySkmuT1QTa9CUVJaaK1n2xUKl54f5Jf0flft2OpjcnOH8Z+UcbL4iFn8q/DKDjcFUoycKkXGS7ncqthZHcWVq6idUuskYDKYQAACTluBniKkaUFdt/Zd2Ybro1wcmbGPRK6ahE6hlOWQwtPhwWv5pdWyrX3yun2ZdcXGhjwUYk4wGyRs7zaOEw7qP1NNRXdmxjUO6xRRqZmSqK3J+zk2JryqSc5u8m7tlqrgoRUUUm2yVk3KRiPZjBJAAAAAAAAPVObi1KLaad01zR5lFSWmeoycXtHQtldrFVSo4hpVOSm9FL38lfzMB1/zh6LRx3JqxdLPZstpNmqeLTnG0ayWku/hmDEzJUvX0bedx8MiO/s5jjsHOhN06kXGS6fuiyVWxsj2iVC6idMuskRzKYQACCQSAQQbfI8+q4WVr71PrB/t2NLKwoXLx4Z0sLkLMd69r+jpWR5lTxEd+m76ax6rwyuXUyql1kW3HyIXx7RZtDEZzXZ3lVLE03GpHXpLqjNTfOqW4s18jGrvj1mjl2eZNUws7NXg+U+hZMXLjcv2VHNwZ48v1/ZrDbNA9U4OTSSu27JLm2RKSivJMYuT0jquyWRLCUryX4sldvt4Kvm5Tun+kXTj8NY9fn2/ZsZc37mmdA8VJqKcm7JK7ZKTb0iJSUVtnMdpc3eKqtr/TjdRXjuWfBx1TD9spfI5f8A5Fnj0vRqDdOeAASQAAAAAAAAQSBra0E9HZMRiVTg5zlaKV27lMhBykoo+hWTUIuUvSOYbRZs8XV37WitIrrbyWjCxvhr0UzkMv8A8izaXg1RuHPAAAAAABGySbleZ1cNPfpya7ro12Zgvx4XLUjZxsqePLcTpWS5xDFU9+LakvVC+qf8FZyMeVMtMuWJlQyIdom0pO7VzXNo+4/AU69N06kU4v8ATyj3XZKuXaJiuqjbFxkcr2iyKeDqWetN+mX7e5ZsTLV0f2U/OwZY8/0brYjJb/8Ak1Fp+RP/ACaHJZf/AM4/5Olw+F/9pr/ou28+7OKWMlpK3JcgCmbf5zuJYana8lebXRdEdbjMbu/kf0cLmMzovij9+zn53yrgkgAAAAAAAAAABIjf9kpbLJkOy069p1bwp/rL2OXl8jGv+MPLO1g8TKz+VnhGTbTOJTqPDx0hF6+WeeOxVGKsftnrl8yUpuqPpFXOscMEkAAAAAAAAAE7J8znhainB6dY9GjWyceN0dM3MPLljz7L19nVMpxkK8Y1IO6a+ztyKrZW65OLLrTbG2CnE2h4MpEzLAU69NwqK65+TJXZKt7iYraY2x6yRgo01CKjFWSVkjxKTk9s9xiorSPZB6Pua4+OHourJ8o6eX0Rlpqds1FGHIujTW5s49jsVKtUlVm7uTuWymtVxUUUW+52zcmYDIYQSAAAAAAAAAAAbPZ3EU6eIi6sU43tryT6M082EpVPqb/H2QhcnNeDqcWrK3K2nYqzLsta8Fb2m2PdabrUGt56uD6vwzqYfI/Euk14OJn8V8snOD8lGxuBq0JbtWEovytPuduu+Fi3Flcux7KnqSIxmMAIAJAAAAAAJBALDshnbw9Tck/w5O3/AKvuc7kcX5Y9o+0dfi834p9JemdJVaXf/BWy3J7PqqyfUAz8GPYAcGPYA5vt1nDq1OBF/JB625Nnf4zG6x+RlW5jL7y+KPpFVOscMAgEgAAAAAAAAAEEgA6JsLnEasPh6ms4r5W3zXYr3JYvxy7x9MtfE5vyR+OXtFyOWdoiZjhqVSD4sYyik3qe65yjL+LMV1cJRfdHG8c4cSXDVobzsvBbqe3RdvZRL+vyPp6MBkMTBJAAAAAAAACIZJ0TY3N+NT4Un+JBfdFb5DF+KfZemW7isz5odJe0WSPNe5zjrk4A0+1GarC4eU/zvSK8s2cSj5rFH6NPOyVRU5fZyOpNybk3dt3bLXGKitIpEpOTbf2eT0eAAAAAAAAAAAAAESeoQcnZJt9lqeXJL2eoxlLwi5bEZDXjWVepFwjFO1+bucbkMuuUOkXs7/FYNsLPkktIvHxD7I4hZCu7bZw6VDhqylU09kuZ0OOo+Szb9I5XLZPxVdV7ZzVllRT2AACAACQAAAAACSVluNlQqxqReqf3XVGG+lW1uLM+NfKmxTR1jKMZSxNJVab949n2ZVLqpVy6su+PfG6HaJJliWld7qS6mIzNpHM9r86+KrWj/pw0XnyWXj8b4obftlQ5TM+azrH0jQHROUAQAAAAAAAAAAAAbbZrAUsRXVOq2lutq3VroaWbdOqHaB0OOx67reszo2X5bQw6tTpQX9z1f3K5bkWWfky204lVS1FGw+IfZGE2TEAU3brLqs5Rqxi5RUbNLWx2OMvjHcWV/msaybU4+SmzpSjzTXurHbU1L0yuShKP5I8Ho8gAEkAAAAAAAEghPQNhlGb1cLLepPnzi9Yv6GvkYtdy0zaxcuzHluD/AMEvM9p8RiI7jahHqo6X92YKePqrfb2zZyOVuuXX0v0aQ6BzACAAAAAAAAAAAAAASctxLpVoVF0kjDfBTrcTZxbPjtjI63SqKUVJcmk/uVCUdPRe4S7JM9kHoz/DeQB8N5AMNalTl8s6cZW0u0j1Gco+meJVxl7RT9tcsw9OjxKdNQnvW05fY6vH5Fs7Ore0cPlcWmurtFaZRzvlZBABJAAAAABBIJABAABABIAAAAAAAAAAABII2D1GDbsk2/YhySW2elCT8JHWtncJNYamp6S3eT5oqWS4u1uJecOMo0xUvejZfDeTAbRIAABCq837gFM/6hVtKcOt2zscTHzKRX+dn/GMSknd/ZWvoAgEgAAAAAAAAAAAAAAAAAAAAAAAAnZJQjUxFOE1eLkro1sqcoVNxNvCrjZdGMvR0eGRYWPKjD/JWnlWv/kXFYVC/wCKJtDBUlJWpwWvZGJ2TftmZU1r0kbSx4MgAMXHj5AHHj5AMUqTbuuTAKjtxklaq41Kcd6ys0tX7nU43JhVtSOJy+JZdqUPoqcMhxTduDNe6sdd5tKX5HCjx+Q3rqZ5bMYpK7p6Jd0Y1yFLaWzK+KyEttGnkraG8vRzmteGfCTyAAAAAAAAAAAAAAAAAACCQSQCATclqOOIpv8AvRgyY9qZbNvCl1vi/wBnXuBLwVEvZ9jSad3yQBl48fIA48fIBFAABMpcl7AHsAiYj1fQAw1VeLXh/wCCY+0eZemcfxMbTku0n/kuNT3FFAtWpy/7MRkMQAAAAAAAAAAAAAAAAAJBCBZMp2TliKKrcRRu3ZW7HMv5JVT66Ozi8Q7q++9CvsbiY+ndl7PUQ5Wt+xZwty/HyZcj2VxDrxdSO5GLTb06dEecnkKnW1D2z3h8VcrVKa0kdMRXy0nmr6X7AEMAAHrhvswBw32YBJhNJJNrkAeuJHugCPWTbutV3APHDfZgHJc8oyhiKkZJp7708MtmJNSpT2UbPg4XyT/sxYTL61Z2hTlL6aHuzIrh+TMdWLba/wCMTb1dkcRCjKrNx+VX3E96VjUXJVSmoxRvS4i6NbnIrx0TkgAAAkAEAAAAAEA+2fPoNk6Z8YCA8JjR1zI8PuYWlBLXcu12bKjky7Wtl7w4dKYr9E3hvszAbR7opp3ei7gGfiR7oA+Tmmmk1ewBG4b7MAcN9mATQAAQqvN+4B5AJWH9P1AMoBrcyy6jUkpTpxlLu1qZIXTgtRZhnRXN7ktinTjFWikl2Wh4cm3tmSMVFaSM1OCleL5OLX3CensmS7LTOR51g3QxE6b6SdvZ6ot2Lb8lSkUTMpdV0okEzbNY2+VbP18Qt6Md2NvU+T8I0r86up62dDG4225bS0azEUJU5OElaSdmjarmpxUkadtbrk4y+jGezGfYpt2WrDaS2yVFt6RIxuAq0bKpBxurq5irvhZ+L2Zbceyr81ojGYwnqnByaiubdkvc8ykoptnqMXJpL7OrZTs7Rjho0qlOMna7bWt35Kvflzla5RZdMfBrjSoSWzQZ3sQruWHlbS/Dl+zNynlWvFi/yc/J4VPzU9fo0mC2XxMqijOG7FNXk+RuW8hSobj5Zz6eKvdmpLSOn4ONtFySS+xW29vZbktLRJBJixHp+oBFAPVPmvcAmgAAAAAhVeb9wDyASsP6fqAZQCPiegBgAMuG5/QAru1+zEsVJVaTSnazT0vbqdLBzlSusvRx+R415DU4eyJk2xUaf4mJtJp6U16fr3MmTycpLrX4Riw+GjB9rfLLPCKSslZLouRym2/Z3EklpFU20yTfXxFNfMl8y7rudTjsvo/jl6OJy2D8kflj7RRCwb15Ktou2xuzLvHE1o6c4Qf+5nEz87f/AK4MsXGcd5Vti/6LTmWAp14unUjddH1T7pnJqtlXLtE7t1ELo9ZI5znuz9TCy0vOm+U0v0ZY8TOhavPhlSzeOnRLa8o2Ow2TOrW404vchqr9X0NfkclRh0i/Zs8ThuVnea8I6YcAtRGxPP6fyAYQDPheoBIAMWI9IBFAPVPmvcAmgAA//9k='}}/>
                        </View>
                        <View transparent style={styles.bestSellerNameLeft} numberOfLines={1}>
                            <View transparent style={styles.bestSellerName}>
                                <Text style={styles.bestSellerName}>{props.sellerName}</Text>
                            </View>
                            <Row transparent style={styles.wholePrice}>
                                <Text style={styles.sellerTitle}>
                                    {props.bestSellerTitleText}
                                </Text>
                                <Row transparent style={styles.priceContainer}>
                                    <Text style={styles.price}>
                                        {props.price}
                                    </Text>
                                    <Text style={styles.toman}>
                                        {props.toman}
                                    </Text>
                                </Row>
                            </Row>
                        </View>
                    </Row>
                    <Row transparent>
                        <Button style={styles.otherSellerButton}>
                            <Text style={styles.otherSellers}>
                                {props.toOtherSellers}
                            </Text>
                        </Button>
                    </Row>
                </View>
            </ScrollView>
        </TemplateProvider>
    );


};
export default ProductItem;

ProductItem.defaultProps = {
    rate: '۴.۵',
    star: '&#9734;',
    like: ' ۵۶',
    dislike: ' ۶',
    year: '۲۰۱۸',
    productType: 'روغن موتور',
    brand: 'کاسترول',
    suggestedBy: '۲۰',
    title: 'روغن موتور خودرو کاسترول مدل مگناچ',
    subtitle: 'Castrol Magnatec 4L 10W40 Car Engine Oil',
    suitableText: 'مناسب برای خودروی :',
    suitableItem: '۲۰۶',
    toOtherSellers: 'نمایش ۲ فروشنده دیگر',
    sellerName: 'پاورتولز',
    bestSellerTitleText: 'فروشنده برتر',
    price: '۳,۴۹۰,۰۰۰',
    toman: 'تومان',
};
