import {StyleSheet, Dimensions} from 'react-native';

const {windowWidth} = Dimensions.get('window');

export const styles = StyleSheet.create({

    container: {
        width: windowWidth,
        backgroundColor: '#f9f9f9',
    },
    ratingContainer: {
        height: 43,
        backgroundColor: '#24aa5b',
        alignItems: 'center',
        padding: 16,
    },
    suggested: {
        color: '#fff',
        flex: 1,
        fontSize: 16,
    },
    starRating: {
        padding: 4,
        margin: 2,
    },
    rate: {
        backgroundColor: 'rgba(255,255,255,0.4)',
        color: '#fff',
    },
    beforeRate: {
        backgroundColor: 'rgba(255,255,255,0)',
        borderTopColor: 'rgba(255,255,255,0.4)',
    },
    afterRate: {
        backgroundColor: 'rgba(255,255,255,0)',
        borderBottomColor: 'rgba(255,255,255,0.4)',
    },
    rateContainer: {
        height: 28,
    },
    imageContainer: {
        height: 300,
        position: 'relative',
        paddingHorizontal: 30,
        paddingVertical: 30,
    },
    mainImage: {
        resizeMode: 'contain',
        height: '100%',
        width: '100%',
    },
    logo: {
        position: 'absolute',
        resizeMode: 'contain',
        height: 23,
        width: 100,
        top: 20,
        left: 16,
    },
    buttonBox: {
        right: 17,
        top: 13,
        position: 'absolute',
        width: 35,
        height: 78,
        justifyContent: 'space-between',
    },
    notifyMe: {
        width: 35,
        height: 35,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#555',
    },
    bellIcon: {
        color: '#555',
    },
    priceHistory: {
        width: 35,
        height: 35,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#555',
    },
    thumbsBox: {
        right: 16,
        bottom: 0,
        position: 'absolute',
        width: 125,
        height: 28,
        justifyContent: 'space-between',
    },
    like: {
        backgroundColor: '#daefff',
        borderWidth: 1,
        borderColor: '#daefff',
        height: 28,
        width: 60,
        flexDirection: 'row',
        alignItems: 'center',
    },
    likeIcone: {
        color: '#3481ff',
        paddingHorizontal: 2,

    },
    disLike: {
        borderColor: '#ededed',
        borderWidth: 1,
        height: 28,
        width: 60,
        backgroundColor: '#e6e6e6',
        flexDirection: 'row',
        paddingBottom: 4,
    },
    disLikeIcone: {
        color: '#444',
        paddingHorizontal: 2,
        //paddingTop: 4,
    },
    likeText: {
        color: '#2286f1',
    },
    dislikeText: {
        color: '#444',
    },
    nameContainer: {
        height: 105,
        padding: 16,
    },
    vLine: {
        borderLeftWidth: 1,
        borderLeftColor: '#ff7200',
    },
    nameBox: {
        flex: 1,
        justifyContent: 'space-between',
        paddingVertical: 5,
        paddingHorizontal: 10,
    },
    titleBox: {
        fontSize: 19,
        fontFamily: 'IRANYekanMobileBold',

    },
    subtitleBox: {
        color: '#999',
        alignSelf: 'flex-end',
    },


    detailsContainer: {
        height: 75,
        alignItems: 'center',
        padding: 16,

    },
    rightDetailsContainer: {
        alignItems: 'center',
        flex: 1,

    },
    sunIcon: {
        color: '#888',
    },
    brand: {
        backgroundColor: '#dcf1ff',
        color: '#2286f1',
    },
    beforeBrand: {
        borderTopColor: '#dcf1ff',
    },
    afterBrand: {
        borderBottomColor: '#dcf1ff',
    },
    year: {
        backgroundColor: '#ffeecf',
        color: '#ff7200',
    },
    beforeYear: {
        borderTopColor: '#ffeecf',
    },
    afterYear: {
        borderBottomColor: '#ffeecf',
    },
    parallContainer: {
        marginHorizontal: 2,
    },
    parallText: {
        fontSize: 16,
        padding: 4,
        paddingHorizontal: 12,
    },
    beforeParallContainer: {
        borderTopWidth: 28,
        borderLeftWidth: 0,
        borderRightWidth: 5,
        borderRightColor: 'transparent',
        borderLeftColor: 'transparent',
    },
    afterParallContainer: {
        borderBottomWidth: 28,
        borderLeftWidth: 5,
        borderRightWidth: 0,
        borderRightColor: 'transparent',
        borderLeftColor: 'transparent',
    },
    leftDetailsContainer: {
        alignItems: 'center',
    },
    productType: {
        flexDirection: 'row',
        borderColor: '#aaa',
        borderWidth: 0.5,
        backgroundColor: '#efefef',
        padding: 15,
        height: 31,
    },

    productTypeText: {
        color: '#555',
    },
    suitableContainer: {
        height: 30,
    },
    suitableFor: {
        fontSize: 16,
        paddingRight: 16,
        paddingLeft: 8,
        color: '#333',
    },
    suitableList: {
        alignItems: 'center',
        height: 30,
        flexDirection: 'row-reverse',
    },
    suitableListItem: {
        height: 29,
        paddingHorizontal: 16,
        marginHorizontal: 6,
        borderColor: '#aaa',
        borderWidth: 0.5,
        backgroundColor: '#fff',
        borderRadius: 3,
    },
    suitableListItemText: {
        fontSize: 15,
        color: '#666',
    },
    bestSeller: {
        backgroundColor: '#fff',
        height: 145,
        margin: 16,
        padding: 16,
        borderColor: '#aaa',
        borderWidth: 0.5,
        borderRadius: 4,
    },
    bestSellerNameContainer: {
        flex: 1,
        paddingBottom: 12,

    },
    otherSellerButton: {
        height: 42,
        flex: 1,
        backgroundColor: '#0076ff',
        padding: 10,
    },
    otherSellers: {
        color: '#fff',
    },
    logoHolder: {
        position: 'relative',
        backgroundColor: '#e6e6e6',
        borderWidth: 0.5,
        borderColor: '#aaa',
        borderRadius: 3,
        height: 55,
        width: 55,
    },
    bestSellerLogo: {
        resizeMode: 'contain',
        height: 53,
        width: 53,
    },
    checkSign: {
        position: 'absolute',
        height: 16,
        width: 16,
        right: -6,
        top: -6,
    },
    bestSellerNameLeft: {
        flex: 1,
        paddingRight: 8,
        justifyContent: 'space-between',
    },
    bestSellerName: {
        fontFamily: 'IRANYekanMobileBold',
        color: '#333',
        fontSize: 15,
    },
    wholePrice: {
        alignItems: 'center',
    },
    sellerTitle: {
        color: '#999',
        fontSize: 13,
    },
    priceContainer: {
        flex: 1,
        alignItems: 'center',
    },

    price: {
        color: '#44ac68',
        fontSize: 21,
        flex: 1,
        paddingHorizontal: 6,
    },
    toman: {
        color: '#999',
        fontSize: 11,
        paddingTop: 4,
    },

});
