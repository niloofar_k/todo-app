import {StyleSheet, Dimensions} from 'react-native';

const {windowWidth} = Dimensions.get('window');

export const styles = StyleSheet.create({

    container: {
        width: windowWidth,
        backgroundColor: '#f9f9f9',
    },
});
