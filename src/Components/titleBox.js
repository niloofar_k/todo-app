import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

function TitleBox() {
    const date = new Date();
    let day_month_year = date.toDateString();
    day_month_year = day_month_year.slice(4, day_month_year.length);

    return (<View style={styles.titleBox}>
        <Text style={{
            ...styles.titleText, ...{
                fontSize: 20,
            },
        }}>ToDo List</Text>
        <Text style={styles.titleText}> {day_month_year} </Text>
        <View
            style={styles.hr}
        />
    </View>);

}

export default TitleBox;

const styles = StyleSheet.create({
    titleBox: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginBottom: 20,
        width: 260,
        justifyContent: 'space-between',
        alignItems: 'baseline',
    },
    hr: {
        borderBottomColor: 'black',
        borderBottomWidth: 2.5,
        width: 260,
    },
    titleText: {
        paddingHorizontal: 15,
        paddingVertical: 8,
        fontSize: 14,
    },
});
