import React, {useState} from 'react';
import {StyleSheet, Picker, TextInput, View} from 'react-native';


function InputBox({categoryList, createTask, inputBoxOpen}) {
    const categoryTitle = Object.keys(categoryList);
    const [textValue, setTextValue] = useState('');
    const [category, setCategory] = useState('no category');


    return (<View style={{
        ...styles.Box, ...{
            display: inputBoxOpen || false
                ? 'flex'
                : 'none',
        },
    }}>
        <TextInput style={styles.inputTask}
                   placeholder={' new todo'}
                   value={textValue}
                   onChangeText={(inputText) => {
                       setTextValue(inputText);
                   }}
                   onSubmitEditing={() => {
                       if (createTask !== undefined && typeof createTask === 'function') {
                           createTask(textValue, category);
                           console.log(category);
                           setTextValue('');
                           setCategory('no category');
                       }
                   }
                   }
        />
        <Picker
            mode={'dropdown'}
            selectedValue={category}
            style={styles.picker}
            onValueChange={(itemValue) => {
                setCategory(itemValue);
            }}>
            <Picker.Item label="-select category-" value="no category"/>
            {categoryTitle && categoryTitle.map((value) => {
                return (
                    <Picker.Item label={value} value={value}/>
                );
            })}

        </Picker>
    </View>);

}

export default InputBox;

const styles = StyleSheet.create({
    Box: {
        borderWidth: 2.4,
        borderBottomWidth: 0,
        borderColor: '#20232a',
        marginBottom: 0,
        width: 250,
        height: 100,
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingVertical: 10,
    },
    inputTask: {
        width: 210,
        height: 30,
        borderWidth: 2.4,
        borderColor: '#aaa',
        padding: 0,
        paddingLeft: 10,
        marginTop: 18,
    },
    picker: {
        padding: 0,
        margin: 0,
        height: 30,
        width: 190,
    },
});


