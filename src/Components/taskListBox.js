import React from 'react';
import {StyleSheet, View, Text, ScrollView, TouchableOpacity} from 'react-native';
import StatusButton from './listComponents/statusBotton';


function TaskListBox({taskList, handleDelete, changeStatus, categoryList, handleInputBox}) {
    const categoryTitle = Object.keys(categoryList);
    const selectedCategory = [];
    categoryTitle.forEach((value) => {
            if (categoryList[value].selected) {
                selectedCategory.push(value);
            }
        },
    );

    return (<ScrollView
            style={styles.taskListBox}>

            {taskList && taskList.map((value, index) => {
                {
                    if (selectedCategory.length !== 0) {

                        if (!selectedCategory.includes(value.category)) {
                            return null;
                        }
                    }

                }
                const categoryColor = value.category !== undefined ?
                    categoryList[value.category].color : 'black'
                ;
                return (
                    <View style={styles.singleTask}>
                        <StatusButton style={styles.status}
                                      changeStatus={changeStatus}
                                      index={index}
                                      status={value.status}
                                      categoryColor={categoryColor}
                        />
                        <Text style={{
                            ...styles.task, ...{
                                textDecorationLine: value.status ?
                                    'line-through' : 'none',
                                textDecorationStyle: 'dotted',
                                color: value.category === 'important'
                                    ? categoryColor
                                    : '#444',
                                textDecorationColor: value.status ?
                                    categoryColor : 'black',
                            },
                        }}
                              key={`RowItemOfTodoIndex${index}`}> {value.title}</Text>
                        <TouchableOpacity
                            onPress={() => {
                                if (handleDelete !== undefined && typeof handleDelete === 'function') {
                                    handleDelete({index});
                                }
                            }}
                        >
                            <Text> &#x2715; </Text>
                        </TouchableOpacity>


                    </View>
                );
            })}
            <View>
                <TouchableOpacity onPress={() => {
                    if (handleInputBox !== undefined && typeof handleInputBox === 'function') {
                        handleInputBox();
                    }
                }}>
                    <Text style={styles.addBtn}>+</Text>
                </TouchableOpacity>
                <View styles={{flexGrow: 1}}>
                </View>
            </View>
        </ScrollView>
    );

}

export default TaskListBox;


const styles = StyleSheet.create({
        taskListBox: {
            flexGrow: 3,
            marginBottom: 20,
            width: 230,
            marginLeft: 30,
            marginTop: 25,
        },
        singleTask: {
            flexDirection: 'row',
            justifyContent: 'flex-start',
            margin: 4,
            flexWrap: 'wrap',
        },
        task: {
            flexGrow: 1,
            fontSize: 16,
            flexDirection: 'row',
        },
        status: {
            flexDirection: 'row',
            justifyContent: 'flex-start',
            alignItems: 'center',
            paddingBottom: 2.5,
            borderWidth: 1.5,
            marginTop: 3,
            marginRight: 4,
            width: 15,
            height: 15,
            borderColor: '#666',
        },
        addBtn: {
            width: 30,
            paddingLeft: 3,
            fontSize: 33,
            color: '#444',
        },
    });
