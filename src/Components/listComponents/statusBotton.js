import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

function statusButton({changeStatus, index, status, style, categoryColor}) {
    return (<TouchableOpacity style={style}
                              onPress={() => {
                                  if (changeStatus !== undefined && typeof changeStatus === 'function') {
                                      changeStatus({index});
                                  }
                              }}>
            <Text style={{color: status ? categoryColor : 'white', fontWeight: 'bold'}}
            > &#x2715; </Text>
        </TouchableOpacity>
    );
}

export default statusButton;
