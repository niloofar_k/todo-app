import React from 'react';
import {TouchableOpacity, StyleSheet, Text, View} from 'react-native';


function filterBox({categoryList, filterCategory}) {
    const categoryTitle = Object.keys(categoryList);
    return (
        <View style={styles.filterBox}>
            {categoryTitle && categoryTitle.map((value, index) => {
                return (<TouchableOpacity key={`filterKey${index}`}
                                          style={{
                                              ...styles.filter, ...{
                                                  backgroundColor: categoryList[value].selected ?
                                                      '#ee0' : '#fff',
                                              },
                                          }}
                                          onPress={() => {
                                              if (filterCategory !== undefined && typeof filterCategory === 'function') {
                                                  filterCategory(value);
                                              }
                                          }}>
                        <View style={{
                            ...styles.colorCircle, ...{
                                backgroundColor: categoryList[value].color,
                            },
                        }}/>
                        <Text style={{
                            fontSize: 16,
                            color: '#444',
                        }}> {value} </Text>
                    </TouchableOpacity>
                );
            })}

        </View>
    );
}

export default filterBox;

const styles = StyleSheet.create({
    filterBox: {
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginBottom: 20,
        width: 260,
        justifyContent: 'space-between',
    },
    filter: {
        flexDirection: 'row',
        borderRadius: 5,
        paddingRight: 5,

    },
    hr: {
        borderBottomColor: 'black',
        borderBottomWidth: 2.5,
        width: 260,
    }
    ,
    titleText: {
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    colorCircle: {
        width: 12,
        height: 12,
        borderRadius: 12,
        marginHorizontal: 5,
        marginTop: 5,

    },

});
