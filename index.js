/**
 * @format
 */
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import Authentication from './src/UIComponent/AuthStack/Authentication';

AppRegistry.registerComponent(appName, () => Authentication);
