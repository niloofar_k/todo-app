import React, {Component} from 'react';
import {StyleSheet, View} from 'react-native';
import TitleBox from './src/components/titleBox';
import TaskListBox from './src/components/taskListBox';
import FilterBox from './src/components/filterBox';
import InputBox from './src/components/inputBox';

class App extends Component {
    constructor() {
        super();
        this.state = {
            inputBoxOpen: false,
            taskList: [
                {
                    title: 'This is a sample task',
                    status: false,
                    category: undefined,
                }, {
                    title: 'This task is important',
                    status: false,
                    category: 'important',
                }, {
                    title: 'This task is for work',
                    status: false,
                    category: 'work',
                }, {
                    title: 'This task is hobby',
                    status: true,
                    category: 'hobby',
                }, {
                    title: 'This sample task is done',
                    status: true,
                    category: undefined,
                }, {
                    title: 'This work task is done',
                    status: true,
                    category: 'work',
                },
            ],
            categoryList: {
                work: {
                    selected: false,
                    color: '#7a3db4',
                },
                hobby: {
                    selected: false,
                    color: '#00e1ff',
                },
                important: {
                    selected: false,
                    color: '#d30431',
                },
            },
        };
    }

    filterCategory = (categoryTitle) => {
        let categoryList = {...this.state.categoryList};
        categoryList[categoryTitle].selected = !categoryList[categoryTitle].selected;
        this.setState({categoryList: categoryList});
    };
    createTask = (value, category) => {
        const copyOfTaskLists = [...this.state.taskList];
        if (value !== '') {
            if (category === 'no category') {
                category = undefined;
            }
            copyOfTaskLists.push({
                title: value,
                status: false,
                category: category || undefined,
            });
            this.setState({
                taskList: copyOfTaskLists,
            });
        }
    };

    handleDelete = (indexDel) => {
        const copyOfTaskLists = [...this.state.taskList];
        copyOfTaskLists.splice(indexDel.index, 1);
        this.setState({taskList: copyOfTaskLists});
    };
    changeStatus = (task_status) => {
        const copyOfTaskLists = [...this.state.taskList];
        copyOfTaskLists[task_status.index].status = !copyOfTaskLists[task_status.index].status;

        this.setState({taskList: copyOfTaskLists});
    };
    handleInputBox = () => {
        this.setState({
            inputBoxOpen: !this.state.inputBoxOpen,
        });
    };

    render() {
        return (
            <View style={styles.background}>
                <View style={styles.container}>
                    <TitleBox/>
                    <TaskListBox taskList={this.state.taskList}
                                 categoryList={this.state.categoryList}
                                 handleDelete={this.handleDelete}
                                 changeStatus={this.changeStatus}
                                 handleInputBox={this.handleInputBox}
                    />
                    <FilterBox categoryList={this.state.categoryList}
                               filterCategory={this.filterCategory}
                    />
                    <InputBox categoryList={this.state.categoryList}
                              createTask={this.createTask}
                              inputBoxOpen={this.state.inputBoxOpen}/>
                </View>

            </View>
        );
    }
}

export default App;

const styles = StyleSheet.create({
    background: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fffbb1',
    },
    container: {
        backgroundColor: '#fff',
        width: 300,
        height: 550,
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        margin: 50,
        marginHorizontal: 80,
        paddingTop: 20,
        borderWidth: 2.4,
        borderColor: '#20232a',
        color: '#20232a',
        textAlign: 'center',
    },
});
